import ProduitCategories from "./ProduitCategories.js";
import Produit from "./Produit.js";
import TypesDePeau from "./TypesDePeau.js";

export { ProduitCategories, Produit, TypesDePeau };
