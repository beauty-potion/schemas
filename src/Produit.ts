import { z } from "zod";
import ProduitCategories from "./ProduitCategories.js";
import TypesDePeau from "./TypesDePeau.js";

const Produit = z.object({
  name: z.string(),
  brand: z.string(),
  price: z.number(),
  category: ProduitCategories,
  typeDePeau: z.nullable(TypesDePeau),
});

export default Produit;
