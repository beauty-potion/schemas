import { z } from "zod";

const ProduitCategories = z.enum([
  "creme",
  "serum",
  "contour yeux",
  "masque",
  "beaume lèvres",
  "nettoyant",
  "lotion",
  "gommage",
]);

export default ProduitCategories;
