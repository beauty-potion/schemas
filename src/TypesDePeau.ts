import { z } from "zod";

const TypesDePeau = z.enum([
  "normale",
  "mixte",
  "seche",
  "grasse",
  "desydratee",
  "sensible",
]);

export default TypesDePeau;
