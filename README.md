# Schemas

## Usage

This package contains all the zod schemas and constants used by the application.
It is not mandatory but very useful to validate and parse the API parameters and response.

## Installation

Since the package is hosted on gitlab it's not accessible via the usual npm registry and you need those two commands.

```
npm config set -- //gitlab.com/:_authToken=a_gitlab_token
npm config set @beauty-potion:registry https://gitlab.com/api/v4/packages/npm/
```
